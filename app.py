import base64
import os

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Output, Input, State

from utils import read_pretrained_w2v, get_most_similar_articles, parse_articles, get_articles_amount

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)


# BASEDIR = '~/Desktop/stories/'
BASEDIR = './articles/'
model = read_pretrained_w2v('./stories_w2v.model')


app.layout = html.Div(children=[
    html.H1(children='Articles searching tool'),
    html.Br(),

    html.H3('Add new article'),
    dcc.Upload(
        id='add-article',
        children=html.Div([
            'Drag and Drop or ',
            html.A('Select Files')
        ]),
        style={
            'width': '100%', 'height': '60px', 'lineHeight': '60px',
            'borderWidth': '1px', 'borderStyle': 'dashed',
            'borderRadius': '5px', 'textAlign': 'center', 'margin': '10px'
        },
    ),

    dcc.Input(
        id='phrase',
        placeholder='Enter phrase',
        style={'width': '400px'}
    ),
    html.Button('Search', id='search', n_clicks=0, style={'margin': 10, 'width': '150px'}),
    html.Span(id='total-articles', children=['(Total articles: {})'.format(get_articles_amount('./articles'))]),

    html.Div(id='result', children=[]),
    html.Div(id='articles')
], style={'margin': 20})


@app.callback(Output('total-articles', 'children'),
              [Input('add-article', 'contents')],
              [State('add-article', 'filename')])
def upload_article(contents, filename):
    if contents is not None:
        content_type, content_string = contents.split(',')
        decoded = str(base64.b64decode(content_string))
        with open(os.path.join('./articles', filename), 'w') as f:
            f.write(decoded)
    return '(Total articles: {})'.format(get_articles_amount('./articles'))


@app.callback(Output('articles', 'children'),
              [Input('search', 'n_clicks')],
              [State('phrase', 'value')])
def search_articles(n_clicks, phrase):
    if n_clicks > 0:
        articles = get_most_similar_articles(phrase, BASEDIR, model)
        articles = parse_articles(articles)
        return articles
    return None


if __name__ == '__main__':
    if not os.path.isdir('articles'):
        os.mkdir('articles')

    app.run_server(debug=True)
