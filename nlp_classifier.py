#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
from collections import Counter
import math

import pandas as pd
import nltk
from nltk.tokenize import RegexpTokenizer
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report


# In[2]:


# create df with texts and its' labels

df = pd.DataFrame(columns=['text', 'label'])

rootdir = 'bbc'
dirs = ['sport', 'politics', 'entertainment', 'business', 'tech']

for directory in dirs:
    subdir = os.path.join(rootdir, directory)
    for file in os.listdir(subdir):
        full_path = os.path.join(subdir, file)
        with open(full_path, 'r', encoding='utf-8', errors='ignore') as f:
            text = f.read()
            df = df.append(pd.DataFrame({"text": [text], "label": [directory]}))


# In[3]:


# split for train and test data

X_train, X_test, y_train, y_test = train_test_split(df[['text']], df['label'], test_size=0.5, random_state=42)


# In[4]:


class NLPModel:
    def __init__(self):
        self.model = RandomForestClassifier(n_estimators=100, max_depth=2, random_state=0)
    
    def _process_text(self, content):
        lem = nltk.stem.wordnet.WordNetLemmatizer()

        stop_words = set(nltk.corpus.stopwords.words("english"))

        tokenizer = RegexpTokenizer(r'\w+')

        tokenized_word = tokenizer.tokenize(content)

        filtered_text = []

        for word in tokenized_word:
            if word not in stop_words and not word.isdigit():
                filtered_text.append(lem.lemmatize(word.lower()))

        return filtered_text
    
    def _prepare_data(self, data):
        
        def get_occurence(data, key):
            occurence = 0
            for item in data:
                if item[key] != 0:
                    occurence += 1
            return occurence
        
        X = data.copy()
        
        X['text'] = X['text'].apply(lambda text: self._process_text(text))
        
        self.unique_words = set()

        for t in X['text']:
            self.unique_words = set(self.unique_words).union(set(t))
            
        df_bag = []

        for t in X['text']:
            row = dict.fromkeys(self.unique_words, 0)
            counts = Counter(word for word in t)
            for key, value in counts.items():
                row[key] += value
            df_bag.append(row)
            
        self.tf = {}
        total = X_train.shape[0]

        for column in self.unique_words:
            occurrence = get_occurence(df_bag, column)
            value = math.log10(total / occurrence)
            self.tf[column] = value

            for i in range(len(df_bag)):
                df_bag[i][column] *= value
            
        df_bag = pd.DataFrame(df_bag)
        
        return df_bag
    
    def _prepare_single(self, v):
        text = self._process_text(v['text'])
        row = dict.fromkeys(self.unique_words, 0)
        counts = Counter(word for word in text)
        for key, value in counts.items():
            if key in self.unique_words:
                row[key] = value * self.tf[key]
        return row
        
    def predict_single(self, v):
        row = self._prepare_single(v)
        return self.model.predict([row])
        
    def predict(self, X):
        data = pd.DataFrame([self._prepare_single(row) for i, row in X.iterrows()])
        return self.model.predict(data)
    
    def train(self, X, y):
        X = self._prepare_data(X)
        self.model.fit(X, y)


# In[5]:


model = NLPModel()
model.train(X_train, y_train)
predictions = model.predict(X_test)


# In[6]:


print(classification_report(y_test, predictions))

