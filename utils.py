import os
from statistics import mean

import nltk
import pandas as pd
import dash_html_components as html
from gensim.models import Word2Vec


BASEDIR = '~/Desktop/stories/'


def train_new_w2v(data_path, save=False):
    print('{}_w2v.model'.format(data_path.split('/')[-1]))
    data = []

    for i, file in enumerate(os.listdir(data_path)):
        if i % 1000 == 0:
            print('Batch {} processing (total {})'.format(i / 1000, i))
        full_path = os.path.join(data_path, file)
        with open(full_path, 'r', encoding='utf-8', errors='ignore') as f:
            text = f.read()
            data.append(process_text(text))

    model = Word2Vec(data, size=150, window=10, min_count=3, iter=10)
    if save:
        model.save('{}_w2v.model'.format(data_path.split('/')[-1]))

    return model


def read_pretrained_w2v(path):
    return Word2Vec.load(path)


def process_text(content):
    lem = nltk.stem.wordnet.WordNetLemmatizer()
    stop_words = set(nltk.corpus.stopwords.words("english"))
    tokenizer = nltk.RegexpTokenizer(r'\w+')

    tokenized_word = tokenizer.tokenize(content)

    filtered_text = []

    for word in tokenized_word:
        if word not in stop_words and not word.isdigit():
            filtered_text.append(lem.lemmatize(word.lower()))

    return filtered_text


def get_similarity(phrase, text, model):
    similarities = []
    text = list(filter(lambda x: x in model.wv.vocab, text))
    words_count = len(phrase)
    for i in range(len(text)):
        j = i + words_count
        if j > len(text):
            break
        part = text[i: j]
        similarity = model.n_similarity(phrase, part)
        if similarity > 0.5:
            similarities.append(similarity)
    return mean(similarities or [0])


def get_most_similar_articles(phrase, articles_dir, model):
    phrase = process_text(phrase)
    articles = []

    for i, file in enumerate(os.listdir(articles_dir)):
        full_path = os.path.join(articles_dir, file)

        with open(full_path, 'r', encoding='utf-8', errors='ignore') as f:
            text = process_text(f.read())
            similarity = get_similarity(phrase, text, model)
            if similarity > 0:
                articles.append({'path': full_path, 'similarity': similarity})
                articles = sorted(articles, key=lambda item: item['similarity'], reverse=True)[: 10]

    return articles


def parse_articles(data):
    children = []

    for i, article in enumerate(data, 1):
        with open(article['path'], 'r', encoding='utf-8', errors='ignore') as f:
            label = html.H4('#{} (similarity: {})'.format(i, article['similarity']))
            text = html.P(f.read().replace('\\n', ' '))
            children.append(label)
            children.append(text)

    return children


def get_articles_amount(path):
    if not os.path.isdir(path):
        return 0
    return len(os.listdir(path))
